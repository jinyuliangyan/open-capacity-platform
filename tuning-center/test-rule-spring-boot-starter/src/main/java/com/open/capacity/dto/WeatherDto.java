package com.open.capacity.dto;

import lombok.Data;

@Data
public class WeatherDto {

	private String name ;
	private Integer value ;
	private String description ;
	private String express ;
	
}
